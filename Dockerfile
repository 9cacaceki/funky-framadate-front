# Create stage to build angular application
FROM node:18.15.0-alpine AS angular-builder

WORKDIR /app

COPY package.json .

RUN yarn install

COPY . .

RUN yarn run build:prod

# Build NGINX Image to serve builded files
FROM nginx:stable-alpine

WORKDIR /app

RUN rm -rf /usr/share/nginx/html/*

COPY docker/nginx/nginx.conf /etc/nginx/nginx.conf

# Copy dist folder fro build stage to nginx public folder
COPY --from=angular-builder /app/dist/framadate /app

# Start NgInx service
CMD ["nginx", "-g", "daemon off;"]